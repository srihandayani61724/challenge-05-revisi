const { Size } = require('../../models');

class sizeController{
    
    static async list(req,res){
        try {
          const sizes = await Size.findAll()

          return res.status(200).json({
              message: "success",
              data: sizes
          })
        } catch (error) {
          console.log(error)  
        }
    }
}

module.exports=sizeController