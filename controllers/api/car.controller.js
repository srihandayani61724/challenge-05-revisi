const { Car } = require('../../models');

class carController{
    
    static async list(req,res){
        try {
          const cars = await Car.findAll()

          return res.status(200).json({
              message: "success",
              data: cars
          })
        } catch (error) {
          console.log(error)  
        }
    }

    static async find(req,res){
        try {
          const cars = await Car.findOne({
              where: {id: req.params.id}
          })

          return res.status(200).json({
              message: "success",
              data: cars
          })
        } catch (error) {
          console.log(error)  
        }
    }

    static async post(req,res){
        try {
          const {name, price, size_id, url_image} = req.body

          const cars = await Car.create({
             name,
             price,
             url_image,
             size_id,
             createdAt: new Date(),
             updatedAt: new Date() 
          })
          res.status(200).json({
              message: "success",
              data: cars
          })
        } catch (error) {
          console.log(error)  
        }
    }

    static async update(req,res){
        try {
          const {name, price, size_id, url_image} = req.body

          const cars = await Car.update({
            name,
            price,
            url_image,
            size_id,
            updatedAt: new Date()
          },
          {
             where: {id: req.params.id}
          })

          return res.status(200).json({
              message: "success",
              data: cars
          })
        } catch (error) {
          console.log(error)  
        }
    }

    static async delete(req,res){
        try {
          const cars = await Car.destroy({
              where: {id: req.params.id}
          })

          return res.status(200).json({
              message: "success",
              data: cars
          })
        } catch (error) {
          console.log(error)  
        }
    }

}

module.exports=carController
