const axios =require("axios");

class carController{
    static async index(req,res){
        const data = {
            listCars: await axios('http://localhost:8080/api/cars').then(res => res.data.data)
        }

        res.render('index',data)
    }

    static async create(req,res){
        const data = {
            sizes: await axios('http://localhost:8080/api/sizes').then(res => res.data.data)
        }

        res.render('car/addCar',data)
    }

    static async update(req,res){
        const data = {
            sizes: await axios('http://localhost:8080/api/sizes').then(res => res.data.data),
            car: await axios(`http://localhost:8080/api/cars/${req.params.id}`).then(res => res.data.data)
        }

        res.render('car/editCar',data)
    }

}

module.exports = carController

