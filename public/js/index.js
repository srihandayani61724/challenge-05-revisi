const btnDelete = document.querySelectorAll('.btn-delete')
const btnConfirm = document.querySelector('.btn-confirm')

btnDelete.forEach(btn => {
    btn.addEventListener('click', function(){
        const carId = this.getAttribute('data-id-car')
        
        btnConfirm.addEventListener('click', function(){
            fetch(`http://localhost:8080/api/cars/${carId}`,{
                method: 'DELETE'
            })
            .then(() => window.location.replace('/'))
        })
    })
})