npm install express axios sequelize pg ejs nodemon
Pages :
Index = http://localhost:8080/
index

Endpoint :
// UPDATE
http://localhost:8080/update/2 = update to data

// CREATE 
http://localhost:8080/create = Create to data

// GET
http://localhost:8080/api/cars       = get all car data from database

// DELETE
http://localhost:8080/api/cars/12= delete car based on car id from database



Contoh request body & response body :
request body : input nama, harga sewa, ukuran mobil, dan url gambar mobil di page list.
response body : Data mobil-mobil yang ditampilkan di halaman awal.

## Directory Structure

```
.
├── config
│   └── config.json
├── controllers
├── migrations
├── models
│   └── index.js
├── public
│   ├── css
│   ├── fonts
│   ├── img
│   ├── js
│   └── uploads
├── seeders
├── views
│   ├── templates
│   │   ├── footer.ejs
│   │   └── header.ejs
│   ├── form.ejs
│   └── list.ejs
├── .gitignore
├── README.md
├── index.js
├── package.json
└── yarn.lock
```

## ERD
![Entity Relationship Diagram]
