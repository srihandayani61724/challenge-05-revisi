const express = require('express');

const carController = require('../../controllers/api/car.controller');
const sizeController = require('../../controllers/api/size.controller');

const router = express.Router();

/* Car Router */
router.get('/cars', carController.list);
router.get('/cars/:id', carController.find);
router.post('/cars', carController.post);
router.post('/cars/:id', carController.update);
router.delete('/cars/:id', carController.delete);

/* Size Router */
router.get('/sizes', sizeController.list);

module.exports = router;

