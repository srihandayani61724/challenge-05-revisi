const express = require('express')
const router = express.Router()

const carController = require('../../controllers/web/car.controller')

router.get('/', carController.index)
router.get('/create', carController.create)
router.get('/update/:id', carController.update)

module.exports = router
