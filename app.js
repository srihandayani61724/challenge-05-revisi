const express = require('express')
const path = require('path');

const route = require('./routes');

const app = express()

const PORT = 8080;

app.set('view engine', 'ejs');

app.use(express.urlencoded())
app.use(express.json())
app.use(express.static(path.join(__dirname, 'public')));

app.use('/', route);

app.listen(PORT, 'localhost', () => {
    console.log(`Server sudah berjalan, silahkan buka http://localhost:${PORT}`);
});




