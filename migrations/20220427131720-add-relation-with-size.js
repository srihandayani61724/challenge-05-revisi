'use strict';

module.exports = {
  async up (queryInterface, Sequelize) {
    /**
     * Add altering commands here.
     *
     * Example:
     * await queryInterface.createTable('users', { id: Sequelize.INTEGER });
     */
     await queryInterface.addColumn('Cars', 'size_id', {
      type: Sequelize.INTEGER,
      references: {
        model: 'Sizes',
        key: 'id'
      },
      allowNull: false,
      foreignKey: true
    });

  },

  async down (queryInterface, Sequelize) {
    /**
     * Add reverting commands here.
     *
     * Example:
     * await queryInterface.dropTable('users');
     */
     await queryInterface.addColumn('Cars', 'size_id');

  }
  
};
